#!/usr/bin/env Rscript

# Pileup visualization for COPRO-Seq Data

setwd(".")

library(rbamtools); packageVersion("rbamtools")
library(xtable); packageVersion("xtable")
library(ggplot2); packageVersion("ggplot2")
library(ape); packageVersion("ape")
library(reshape2); packageVersion("reshape2")
library(ggridges); packageVersion("ggridges")

# Source functions --------------------------------------------------------

source("/home/hibberdm/coproseq_pileup/coproseq_pileup_functions.R")

# Constants ---------------------------------------------------------------

datestring <- format(Sys.time(), "%y%m%d_%H%M")
experiment <- "coproseq_pileup"
study <- "patnode"
adjust_coords <- F

target_ref <- "*" # use this setting to restrict analyses to a particular contig

# Arguments ---------------------------------------------------------------

args = commandArgs(trailingOnly=TRUE)
if (length(args)==3) {
	target_coords <- c(as.numeric(args[2]), as.numeric(args[3]))
} else {
	target_coords <- c(-Inf, Inf)
}
target_ref <- "*" # use this setting to restrict analyses to a particular contig

# Open gene position file -------------------------------------------------

# Read .gff file from prokka annotation (preferred)
gff_file <- paste("genomes", args[1], sep = "/")
gff_base <- gsub(pattern = "\\.gff$", "", basename(gff_file))
if (file.exists(paste("genomes/", gff_base, "_processed.gff", sep = ""))) {
  gff <- readRDS(file = paste("genomes/", gff_base, "_processed.gff", sep = ""))
} else {
  gene_lines <- system(paste("grep -c \".*ID=\" ", gff_file, sep = ""), intern = T)[1]
  gff <- read.table(gff_file, sep = "\t", nrows = as.numeric(gene_lines), quote = "")
  colnames(gff) <- c("seqname", "source", "feature", "start", "end", "score", "strand", "frame", "attribute")
  gff <- cbind(gff, process_attr(gff))
  saveRDS(gff, file = paste("genomes/", gff_base, "_processed.gff", sep = ""))
}

# Adjust feature coordinates to cumulative (for concatenated contigs)
if (adjust_coords) {
  gff <- adjust_feature_coords(gff)
} else {
  gff$start_adj <- gff$start
  gff$end_adj <- gff$end
}

# Query mapped dir for bam files ------------------------------------------

bam_dir <- paste(getwd(), "mapped", sep = "/")
bam_files <- list.files(bam_dir, full.names = T, pattern = "*\\.bam$")

# Loop through bam results, generating depth data and plots for each contig

plot_dir <- paste(getwd(), "plots", sep = "/")
dir.create(path = plot_dir)

ref_depth_agg <- data.frame(ref = character())

for (bam in bam_files) {
  bam_base <- gsub(pattern = "\\.bam$", "", basename(bam))
  reader <- bamReader(bam)
  if (file.exists(paste(bam, "bai", sep = "."))) {
    loadIndex(reader, paste(bam, "bai", sep = "."))
  } else {
    createIndex(reader, idx_filename = paste(bam, "bai", sep = "."))
    loadIndex(reader, paste(bam, "bai", sep = "."))
  }
  if (indexInitialized(reader)) {
    refdata <- getRefData(reader)
    ref_depth_bam_agg <- data.frame(ref = character(), pos = numeric(), depth = numeric())
    ref_depth_stat <- data.frame(ref = character(), mean = numeric(0), median = numeric(0), min = numeric(0), max = numeric(0))
#    pdf(file = paste(plot_dir, "/", datestring, gff_base, "_", bam_base, "_depth.pdf", sep = ""))
    for (ref in refdata$SN) {
      if (grepl(target_ref, ref)) {
        coords <- getRefCoords(reader, ref)
        if (target_coords[1] != -Inf) {
          coords[2] <- target_coords[1]  
        }
        if (target_coords[2] != Inf) {
          coords[3] <- target_coords[2]  
        }
        
        range <- bamRange(reader, coords)
        #  getAlignRange(range) # range of alignments in ref
        
        ad <- alignDepth(range)
        
        depth <- getDepth(ad)
        ref_depth_stat <- rbind(ref_depth_stat, data.frame(ref, mean(depth), median(depth), min(depth), max(depth)))
        
        if (coords[3] - coords[2] > 5000) {
          breaks <- 1000
        } else {
          breaks <- 500
        }
        
        dfr_pos <- data.frame(pos = seq(coords[2], coords[3], 1), depth = depth)
        dfr_pos$ref <- ref
        ref_depth_bam_agg <- rbind(ref_depth_bam_agg, dfr_pos)
        
        p <- ggplot(dfr_pos, aes(x = pos, y = depth, height = depth)) +
#          geom_bar(stat="identity") +
#          geom_ridgeline() +
          geom_density_ridges(stat="identity") +
          scale_y_continuous(expand = c(0,0)) +
          scale_x_continuous(expand = c(0,0), breaks = seq(coords[2], coords[3], breaks)) +
          xlab("reference position") +
          ylab("depth") +
          theme_bw() +
          theme(axis.text.x = element_text(angle=-90, hjust=0))
        print(p)
      }
    }
#    dev.off()    
    write.table(ref_depth_stat, file = paste(datestring, study, experiment, gff_base, "ref_depth_stat.txt", sep = "_"), sep = "\t", row.names = F, quote = F)
    colnames(ref_depth_bam_agg)[colnames(ref_depth_bam_agg) == "depth"] <- bam_base
    ref_depth_agg <- merge(ref_depth_agg, ref_depth_bam_agg, by = "ref", all.y = T)
    
  }
}

